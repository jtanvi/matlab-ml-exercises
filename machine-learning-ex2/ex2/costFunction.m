function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));


prod = X * theta;
h = sigmoid(prod);
term1 = y .* (log(h));
term2 = (1 - y) .* (log (1 - h));
term = - term1 - term2;
summa = sum(term(:));

J = (1/m) * summa;

n = length(theta);
grad = zeros(n,1);

for i = 1:n
    
    gsumma = sum((h - y) .* X(:,i));
    grad(i) = (1/m) * gsumma;

end

% =============================================================

end
