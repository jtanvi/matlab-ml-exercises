function g = sigmoid(z)
%SIGMOID Compute sigmoid function
%   g = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly 
%g = zeros(size(z));

g = arrayfun(@sigmoidValue, z);



% =============================================================

end

function val = sigmoidValue(x)
    
    val = 1 / (1 + (exp(1) ^ (-x)));

end