function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));



% ------------------------------------------------------------
% Computing Cost

prod = X * theta;
h = sigmoid(prod);
term1 = y .* (log(h));
term2 = (1 - y) .* log(1 - h);
term = - term1 - term2;
summa = sum(term);
regterm = (lambda / (2 * m)) * (sum(theta .^ 2) - (theta(1) .^ 2));
J = ((1/m) * summa) + regterm;

% ------------------------------------------------------------
% COmputing gradient vector

grad(1) = (1/m) * sum((h - y) .* X(:,1));
n = length(theta);

for i = 2:n
    
    gsumma = sum((h - y) .* X(:,i));
    grad(i) = ((1/m) * gsumma) + ((lambda/m)*theta(i));
    
end

% ------------------------------------------------------------


% =============================================================

end
