function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples
n = length(theta);

% need to return the following variables correctly 
J = 0;


summa = 0;

for i = 1:m
  h = X(i,:)' .* theta;
  h = sum(h,1);
  val = (h - y(i,1)) ^ 2;
  summa = summa + val;
end

J = (1 / (2 * m)) * summa;



% =========================================================================

end
