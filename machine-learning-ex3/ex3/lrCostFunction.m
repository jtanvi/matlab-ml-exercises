function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));


%       The computation of the cost function and gradients can be
%       efficiently vectorized. For example, consider the computation
%
%           sigmoid(X * theta)
%
%       Each row of the resulting matrix will contain the value of the
%       prediction for that example. You can make use of this to vectorize
%       the cost function and gradient computations. 
%
%       When computing the gradient of the regularized cost function, 
%       there're many possible vectorized solutions, but one solution
%       looks like:
%           grad = (unregularized gradient for logistic regression)
%           temp = theta; 
%           temp(1) = 0;   % because we don't add anything for j = 0  
%           grad = grad + YOUR_CODE_HERE (using the temp variable)
%

h = sigmoid(X * theta);
term = -(y .* log(h)) - ((1-y) .* log(1-h));
summa = sum(term);
%thetasqr = theta .^ 2;
regterm = (lambda / (2 * m)) * (sum(theta .^ 2) - (theta(1) .^ 2));
J = ((1/m) * summa) + regterm;

beta = h - y;
grad = (1/m) * (X' * beta);
temp = theta;
temp(1) = 0;
temp = (lambda/m) .* temp;
grad = grad + temp;

% =============================================================

grad = grad(:);

end
